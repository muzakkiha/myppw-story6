import requests
import json

from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout as auth_logout
from .models import LaptopWish
import os

response = {}
api_laptop_list_url = "https://enterkomputer.com/api/product/notebook.json"

@login_required
def story89(request):
    laptop_list = requests.get(api_laptop_list_url).json()[:20]
    laptopwish_list = LaptopWish.objects.all()
    total_harga = 0

    for i in laptopwish_list:
        total_harga += int(i.price)

    laptops = LaptopWish.objects.all()
    laptopid_list = []
    for i in laptops:
        laptopid_list.append(i.laptop_id)

    response["laptop_list"] = laptop_list
    response["total_harga"] = total_harga
    response["laptopid_list"] = laptopid_list

    html = "story89.html"
    return render(request, html, response)

@csrf_exempt
def add_wish(request):
    if (request.method == "POST"):
        laptop_id = request.POST["laptop_id"]
        name = request.POST["name"]
        price = request.POST["price"]
        laptop_wish = LaptopWish(laptop_id=laptop_id, name=name, price=price)
        laptop_wish.save()
        return JsonResponse(laptop_wish.as_dict())

@csrf_exempt
def remove_wish(request):
    if (request.method == "POST"):
        laptop_id = request.POST["laptop_id"]
        LaptopWish.objects.filter(laptop_id=laptop_id).delete()
        return JsonResponse({"laptop_id": laptop_id})

def login(request):
    return render(request, 'login.html', response)

def logout(request):
    auth_logout(request)
    return HttpResponseRedirect('/story89/')