from django.db import models

class LaptopWish(models.Model):
    laptop_id = models.CharField(max_length=50)
    name = models.CharField(max_length=500)
    price = models.IntegerField()

    def __str__(self):
        return self.laptop_id

    def as_dict(self):
        return {
            "laptop_id": self.laptop_id,
            "name": self.name,
            "price": self.price,
        }