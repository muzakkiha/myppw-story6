from django.urls import path
from .views import story89, add_wish, remove_wish, login, logout
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path("", story89, name="story89"),
    path("add-wish/", add_wish, name="add-wish"),
    path("remove-wish/", remove_wish, name="remove-wish"),
    path("login/", login, name="login"),
    path("logout/", logout, name="logout"),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)