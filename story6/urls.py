from django.contrib import admin
from django.contrib.auth import views
from django.urls import path
from django.conf.urls import include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('', include('home.urls')),
    path('story89/', include('story89.urls')),
    path('story10/', include('story10.urls')),
    path('auth/', include('social_django.urls', namespace='social')),
]
