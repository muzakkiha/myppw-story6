$('#add_user').on('submit', function() {
    event.preventDefault();
    console.log("jalan");
    $.ajax({
        method: 'POST',
        url : '/story10/add-user/',
        data: {
            email: $('#field_email').val(),
            nama: $('#field_nama').val(),
            password: $('#field_password').val(),
            csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
        },
        success : function () {
            alert('Created new user');
        },
        error : function (error) {
            alert(error.responseText);
        }
    });
});

$('#field_email').change(function () {
    email = $(this).val();
    $.ajax({
        method: 'POST',
        url: '{% url "validate-email" %}',
        data: {
            'email': email
        },
        dataType: 'json',
        success: function (data) {
            if (data.is_taken) {
                alert('Email sudah terdaftar');
            }
        }
    });
});