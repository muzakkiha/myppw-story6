from django.urls import path
from .views import story10, add_user, delete_all_user, validate_email, delete_user

urlpatterns = [
    path("", story10, name="story10"),
    path("add-user/", add_user, name="add-user"),
    path("delete-all-user/", delete_all_user, name="delete-all-user"),
    path("delete-user/", delete_user, name="delete-user"),
    path("validate-email/", validate_email, name="validate-email"),
]