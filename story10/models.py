from __future__ import unicode_literals

from django.db import models

class User(models.Model):
    email = models.EmailField()
    nama = models.CharField(max_length=100)
    password = models.CharField(max_length=100)

    def __str__(self):
        return self.email

    def as_dict(self):
        return {
            "email": self.email,
            "nama": self.nama,
            "password": self.password,
        }