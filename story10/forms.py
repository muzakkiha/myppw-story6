from django import forms
from django.core.validators import validate_email

class UserForm(forms.Form):
    email = forms.EmailField(label="Email", required=True, widget=forms.EmailInput(attrs={"class" : "form-control", "id" : "field_email"}))
    nama = forms.CharField(label="Nama", required=True, max_length=100, widget=forms.TextInput(attrs={"class" : "form-control", "id" : "field_nama"}))
    password = forms.CharField(label="Password", required=True, max_length=100, widget=forms.PasswordInput(attrs={"class" : "form-control", "id" : "field_password"}))