from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .forms import UserForm
from .models import User

response = {}

def story10(request):
    response["user_list"] = User.objects.all()
    response["user_form"] = UserForm

    html = "story10.html"
    return render(request, html, response)

@csrf_exempt
def add_user(request):
    form = UserForm(request.POST or None)
    if(request.method == "POST" and form.is_valid()):
        email = request.POST["email"]
        nama = request.POST["nama"]
        password = request.POST["password"]
        user = User(email=email, nama=nama, password=password)
        user.save()
        return JsonResponse(user.as_dict())

def delete_all_user(request):
    User.objects.all().delete()
    return HttpResponseRedirect("/story10/")
    
def delete_user(request, email):
    User.objects.filter(email=email).delete()
    return HttpResponseRedirect("/story10/")

@csrf_exempt
def validate_email(request, email):
    email = request.POST.get("email", None)
    data = {
        "is_taken" : User.objects.filter(email=email).exists()
    }
    return JsonResponse(data)