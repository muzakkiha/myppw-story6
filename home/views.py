from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Status_Form
from .models import Status

response = {}
welcome_words = 'Hello apa kabar?'
status_title = 'Status'

# Create your views here.

def tulis_status(request):
    response['welcome_words'] = welcome_words
    response['status_form'] = Status_Form
    status_list = Status.objects.all()
    response['status_list'] = status_list
    html = 'home.html'
    return render(request, html, response)

def post_status(request):
	form = Status_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		status = Status(status = request.POST['status'])
		status.save()
		return HttpResponseRedirect('/')

def delete_all_status(request):
	Status.objects.all().delete()
	return HttpResponseRedirect('/')