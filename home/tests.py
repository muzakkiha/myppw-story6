from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest

from .models import Status
from .forms import Status_Form
from .views import tulis_status, welcome_words

import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.

class HomeUnitTest(TestCase):

	def test_page_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code,200)

	def test_using_tulis_status_func(self):
		found = resolve('/')
		self.assertEqual(found.func, tulis_status)

	def test_title_is_ready(self):
		request = HttpRequest()
		response = tulis_status(request)
		html_response = response.content.decode('utf8')
		self.assertIn('<title>' + welcome_words + '</title>', html_response)
		self.assertIn('<h1 id="page_head">' + welcome_words + '</h1>', html_response)
		self.assertFalse(len(welcome_words) == 0)

	def test_model_can_create_status(self):
		status_test = Status.objects.create(status='hari ini aku ceria')
		status_count = Status.objects.all().count()
		self.assertEqual(status_count, 1)

	def test_form_status_has_placeholder(self):
		form = Status_Form()
		self.assertIn('class="form-control', form.as_p())

'''class HomeFunctionalTest(TestCase):

	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		service_log_path = "./chromedriver.log"
		service_args = ['--verbose']
		self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		self.browser.implicitly_wait(25)
		super(HomeFunctionalTest, self).setUp()


	def tearDown(self):
		self.browser.quit()
		super(HomeFunctionalTest, self).tearDown()

	def test_input_status(self):
		browser = self.browser
		browser.get('http://127.0.0.1:8000/')

		status_text = browser.find_element_by_name('status')
		status_form = browser.find_element_by_id('form')

		status_text.send_keys('coba coba')
		status_form.submit()

		assert "coba coba" in browser.page_source

	def test_status_title(self):
		browser = self.browser
		browser.get('http://127.0.0.1:8000/')
		assert "Hello apa kabar?" in browser.title

	def test_page_head(self):
		browser = self.browser
		browser.get('http://127.0.0.1:8000/')
		browser_head = browser.find_element_by_id("page_head")
		assert "Hello apa kabar?" in browser_head.get_attribute('innerHTML')

	def test_page_head_uses_css(self):
		browser = self.browser
		browser.get('http://127.0.0.1:8000/')
		page_head = browser.find_element_by_id("page_head")
		assert "page_head" in page_head.get_attribute("id")

	def test_tulis_status_uses_css(self):
		browser = self.browser
		browser.get('http://127.0.0.1:8000/')
		h2 = browser.find_element_by_tag_name("h2")
		assert "tulis-status" in h2.get_attribute("class")'''