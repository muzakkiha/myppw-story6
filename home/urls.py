from django.urls import path
from .views import tulis_status, post_status, delete_all_status

urlpatterns = [
    path('', tulis_status, name='tulis_status'),
    path('post_status/', post_status, name='post_status'),
    path('delete_all_status/', delete_all_status, name='delete_all_status'),
]