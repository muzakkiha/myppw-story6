from django import forms

class Status_Form(forms.Form):
    status = forms.Field(label='', required=True, widget=forms.Textarea(attrs={'class': 'form-control'}))